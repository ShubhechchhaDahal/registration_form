

function myRegistration() {
    const fname = document.getElementById("fullName").value;
    const uname= document.getElementById("userName").value;
    const email = document.getElementById("Email").value;
    const number = document.getElementById("phoneNum").value;
    const pass= document.getElementById("Password").value;
    const confpass= document.getElementById("ConfirmPass").value;
    let passIsValid = validatePassword(pass);
    let passMatch = validatePasswordMatch(pass, confpass);
    if(passIsValid === true && passMatch === true) {
        let formData = {fname, uname, email, number, pass};
        sendDataToServer(formData);
    }else {
        alert("form is invalid");
    }
}

function validatePassword(pass) {
    var pswrd = /([0-9].*[a-z])|([a-z].*[0-9])/;
   
    if(pass.match(pswrd) == null)
    {   
        const errmsg = document.getElementById("errorMessage");
        errmsg.style.display = "block";
        errmsg.innerText = "Password needs to be alphanumeric!";
        return false;
    }
    return true;
}


function validatePasswordMatch(pass, confpass) {     
    if(pass != confpass) {
        const errmsg = document.getElementById("errorMessage");
        errmsg.style.display = "block";
        errmsg.innerText = "Passwords do not match!";
        return false;
    }
    return true;
}

function sendDataToServer(formData) {
    fetch("https://httpbin.org/post", {
        method: "POST",
        body : JSON.stringify(formData)
    }).then ( result => {
        return result.json();
    }).then (response => {
        console.log("Received data is " +response.data);
    }).catch (error => {
        console.log("Error sending the datas" +error.message);
    })
}